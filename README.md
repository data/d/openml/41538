# OpenML dataset: conference_attendance

https://www.openml.org/d/41538

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Data set shows information about participants of math conference. isPresent is target column for classification task.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/41538) of an [OpenML dataset](https://www.openml.org/d/41538). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/41538/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/41538/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/41538/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

